function toggleVisibilityFrench() {
  var div = document.getElementById("maDiv");
  var bouton = document.getElementById("boutonDiv");
  console.log(div)
  if (div.style.display === "none") {
    div.style.display = "block";
    bouton.innerHTML = "Masquer les expériences";
  } else {
    div.style.display = "none";
    bouton.innerHTML = "Afficher plus d'expériences";
  }
}

function toggleVisibilityEnglish() {
  var div = document.getElementById("maDiv");
  var button = document.getElementById("boutonDiv")
  console.log(div)
  if (div.style.display === "none") {
    button.textContent = "Less experiences"
    div.style.display = "block";
  } else {
    button.textContent = "More d'expériences"
    div.style.display = "none";
  }
}

function toggleVisibilityProjets() {
  var div = document.getElementById("projetsDiv");
  var bouton = document.getElementById("boutonProjets");
  if (div.style.display === "none") {
    div.style.display = "block";
    bouton.innerHTML = "Masquer les projets";
  } else {
    div.style.display = "none";
    bouton.innerHTML = "Afficher plus de projets";
  }
}